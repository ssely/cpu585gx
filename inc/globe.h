#ifndef _GLOBE_H
#define _GLOBE_H
#ifndef _WINDOWS_
#include <windows.h>
#include <rttarget.h>
#include <rtk32.h>
#include <webapi.h>
#endif

#define INIT 0x1
#define READ 0x2
#define SHUTDOWN 0x4

typedef struct{
DWORD RegDelay; // ������� �������������;
DWORD isOn; // 1- ���� ����� �������, 0- ���� ��������
DWORD dwWork; //  ����� ������������ �������
WORD BASE;
BYTE Onchannel; //����� ��������� ����� ���/����
BYTE Spdchannel; //����� �������� ����� 
BYTE IdChannel; // ����� �������������� ������� 
BYTE CarCount; // ���-�� ������� � �����
WORD cCount; // ������� ������� ����������
float cSpeed; // �������� 
float EqBaseOne; // ���� ������� (1) 400 ��
float EqBaseTwo; // ���� ������� (2) 300 ��
SYSTEMTIME StopTime; // ����� ���������  ����������� �����
SYSTEMTIME StartTime; //����� ������ ���� ����������� �����
RTKTime WaitTime; // ����� �������
char *WebRoot;
char *FtpRoot;
char *ipaddr;
char *dns;
char *gateway;
char *mask;
char *Buffer;
BYTE *Mp;

}Config, *pConfig;

typedef short  vArray[2][100][8];
typedef float  rArray[16][1200];

typedef struct{
BYTE isActive; // ���������� ������
BOOL State;  // ���-1, ����-0 
BYTE bRange; // �������� ����� 0-125 �� 1-250 ��
BYTE bType; // ��� ������� 0/1 0-20mA/4-20mA
BYTE bReverse; // ������/����������� ������ 0/1
BYTE bFType; // ��� �������
float Fs; // �������  ������ �����������
float Fx; // �������  ������ ���������
WORD wWindow;// ������ ���� ����������� ������� 1024 - ������������
WORD wOffset; // �������� ����� ���������� ������ � ���������
WORD bMin; // ����������� ���������� ������� � ��
WORD bMax; // ������������ ���������� ������� � ��
WORD wSelf; // ����� ������������
WORD wSpline; //����� �����������
RTKTime UpTime; // ����� ������ ��������� �������
float fData[2]; // 0- ��������� �� ������ ��� 1- ��������� �� ������ ���
float isAvg; // ������ ���������� �������� � ������
float isMin; // ����������� ���������� ��������
float coeff; // ����������� ��������� � ��
float *pData; // ������ � ������ � ���
float *pFlt; // ������������� ������ � ������
}Sensor, *pSensor;

typedef struct{
WORD Id;
WORD isError;
SYSTEMTIME isT;
RTKTime Diff;
float lcode[8];
float lparms[8];
float ldhk[4],lrzpk[4],lrzpr[4];
float rcode[8];
float rparms[8];
float rdhk[4],rrzpk[4],rrzpr[4];
}Car, *pCar;


#ifndef _SYS_LIB_C
extern pConfig isValue;
extern BOOL R_TRIG32(BYTE ,BOOL);
extern BOOL F_TRIG32(BYTE ,BOOL);
extern void _setBit(unsigned long * , unsigned char );
extern void _resetBit(unsigned long * , unsigned char);
extern void FilterHz (float * , float * , float , float , WORD , WORD , WORD);
extern void FilterLP(float *, float * , float , WORD);
extern void _fillData(rArray, vArray, pSensor, pConfig, int);
#endif //_SYS_LIB_C
#ifndef _READCFG_C
extern BOOL _readconfig(char *, pConfig);
extern BOOL _clearconfig(pConfig);
#endif
#ifndef _NETUP_C
extern void NetUp(LPSTR ,LPSTR , LPSTR, LPSTR);
extern void WebRun(LPSTR,LPSTR);
#else
extern void copy_fun(PIO_CONTEXT , PFCHAR );
extern void settings_fun(PIO_CONTEXT , PFCHAR );
extern void current_fun(PIO_CONTEXT, PFCHAR);
extern void getall_fun(PIO_CONTEXT, PFCHAR);
extern void reset_fun(PIO_CONTEXT , PFCHAR );
extern void demon_fun(PIO_CONTEXT , PFCHAR );
#endif
#ifndef _NETFUNC_C
extern BOOL InitNetFunc(pConfig, pSensor, pCar);
extern void CloseNetFunc(void);
#endif
#ifndef _MWORKS_C
extern int watch_cycle(pConfig , pSensor , pCar ,int);
extern BOOL fill_cars(pConfig , pSensor , pCar);
#endif
#ifndef _ADC_C
extern RTKSemaphore EndIRQ;
extern int _getADCAddr(WORD* ,WORD* );
extern void SetupADC(int, WORD, WORD ,WORD );
extern void CleanADC(int, WORD, WORD);
extern void _InitADCModule(short *);
#endif
#ifndef _POLLING_C
extern BOOL _InitPolling(pConfig, pSensor, pCar, short *, float *, WORD, WORD);
extern void RTKAPI Polling(void *);
extern void _ClearPolling(void);
#else
extern BOOL _getFtpData(short *,BYTE);
#endif
#ifndef _MAIN_C
extern BOOL _getFtpData(short *, BYTE);
#endif
#endif //_GLOBE_H
