
#ifndef _FFD_H
#define _FFD_H

#define SPARE_AREA_SIZE	16
#define FFD_SECTOR_SIZE 512
#define SECTOR_SIZE     FFD_SECTOR_SIZE

#ifdef _FFD_DEBUG
extern int show_phis_info;
#endif

//extern int FFD_BLOCK_COUNT;
#define MAX_FFD_BLOCK_COUNT 2048


#ifdef _NT_KERNEL_

#include <ntddk.h>
#include <ntdddisk.h>

#ifdef _FFD_DEBUG

#define DEBUG_PRINT( text ) { DebugPrint( text ); };
#define DEBUG_PRINT1( text, par1 ) { DebugPrint( text, par1 ); };
#define DEBUG_PRINT2( text, par1, par2 ) { DebugPrint( text, par1, par2 ); };
#define DEBUG_PRINT3( text, par1, par2, par3 ) { DebugPrint( text, par1, par2, par3 ); };
#define DEBUG_PRINT4( text, par1, par2, par3, par4 ) { DebugPrint( text, par1, par2, par3, par4 ); };
#define DEBUG_PRINT5( text, par1, par2, par3, par4, par5 ) { DebugPrint( text, par1, par2, par3, par4, par5 ); };
#define DEBUG_PRINT6( text, par1, par2, par3, par4, par5, par6 ) { DebugPrint( text, par1, par2, par3, par4, par5, par6 ); };

#else

#define DEBUG_PRINT( text )
#define DEBUG_PRINT1( text, par1 )
#define DEBUG_PRINT2( text, par1, par2 )
#define DEBUG_PRINT3( text, par1, par2, par3 )
#define DEBUG_PRINT4( text, par1, par2, par3, par4 )
#define DEBUG_PRINT5( text, par1, par2, par3, par4, par5 )
#define DEBUG_PRINT6( text, par1, par2, par3, par4, par5, par6 )

#endif _FFD_DEBUG


#endif // _NT_KERNEL_

#ifdef _CE_KERNEL_
#define DEBUG_PRINT( text )
#define DEBUG_PRINT1( text, par1 )
#define DEBUG_PRINT2( text, par1, par2 )
#define DEBUG_PRINT3( text, par1, par2, par3 )
#define DEBUG_PRINT4( text, par1, par2, par3, par4 )
#define DEBUG_PRINT5( text, par1, par2, par3, par4, par5 )
#define DEBUG_PRINT6( text, par1, par2, par3, par4, par5, par6 )
#endif

#ifdef _RTOS_

#include <rtkernel.h>

void LogOutputToCOM( void* data, unsigned len );

#ifdef _FFD_DEBUG

#define DEBUG_PRINT( text ) { char buf[256]; LogOutputToCOM( buf, sprintf( buf, text ) ); };
#define DEBUG_PRINT1( text, par1 ) { char buf[256]; LogOutputToCOM( buf, sprintf( buf, text, par1 ) ); };
#define DEBUG_PRINT2( text, par1, par2 ) { char buf[256]; LogOutputToCOM( buf, sprintf( buf, text, par1, par2 ) ); };
#define DEBUG_PRINT3( text, par1, par2, par3 ) { char buf[256]; LogOutputToCOM( buf, sprintf( buf, text, par1, par2, par3 ) ); };
#define DEBUG_PRINT4( text, par1, par2, par3, par4 ) { char buf[256]; LogOutputToCOM( buf, sprintf( buf, text, par1, par2, par3, par4 ) ); };
#define DEBUG_PRINT5( text, par1, par2, par3, par4, par5 ) { char buf[256]; LogOutputToCOM( buf, sprintf( buf, text, par1, par2, par3, par4, par5 ) ); };
#define DEBUG_PRINT6( text, par1, par2, par3, par4, par5, par6 ) { char buf[256]; LogOutputToCOM( buf, sprintf( buf, text, par1, par2, par3, par4, par5, par6 ) ); };

#else

#define DEBUG_PRINT( text )
#define DEBUG_PRINT1( text, par1 )
#define DEBUG_PRINT2( text, par1, par2 )
#define DEBUG_PRINT3( text, par1, par2, par3 )
#define DEBUG_PRINT4( text, par1, par2, par3, par4 )
#define DEBUG_PRINT5( text, par1, par2, par3, par4, par5 )
#define DEBUG_PRINT6( text, par1, par2, par3, par4, par5, par6 )

#endif _FFD_DEBUG


#endif

#pragma pack( 1 )

extern unsigned char FFD_PAGES_PER_BLOCK;

typedef struct SPARE_AREA
{
	unsigned char	reserved1[5];
	unsigned char	badBlock;		// should be 0xFF is block is OK
	unsigned short	logicalNumber;	// logical number of current block
	unsigned char	serialNumber;	// serial number of record
	unsigned char	signature;		// 0x0A5 for used sector
	unsigned short	CRC;			// check sum
	unsigned long	numWrites;

} SPARE_AREA, *PSPARE_AREA;

#define FFD_VALID_BLOCK_SIGNATURE 0xA5

#ifdef _NT_KERNEL_

#include "ffd_ioctrl.h"

#endif



// NAND access macroses

#ifdef _NT_KERNEL_

#define WRITE_PORT( port, value ) WRITE_PORT_UCHAR( (PUCHAR)port, value)
#define READ_PORT( port ) ( READ_PORT_UCHAR( (PUCHAR) port ) )

#endif // _NT_KERNEL_

#ifdef _RTOS_

#define WRITE_PORT( port, value ) RTOut( port, value )
#define READ_PORT( port ) RTIn( port )

#endif // _RTOS_

#ifdef _CE_KERNEL_

#define WRITE_PORT( port, value ) WRITE_PORT_UCHAR( (PUCHAR)port, value)
#define READ_PORT( port ) ( READ_PORT_UCHAR( (PUCHAR) port ) )

#endif // _CE_KERNEL_


#define NAND_BASE_ADDRESS 0x0300

#define WRITE_NAND_0( value ) WRITE_PORT( NAND_BASE_ADDRESS + 0, value )
#define WRITE_NAND_1( value ) WRITE_PORT( NAND_BASE_ADDRESS + 1, value )

#define READ_NAND_0() (READ_PORT( NAND_BASE_ADDRESS + 0 ))
#define READ_NAND_1() (READ_PORT( NAND_BASE_ADDRESS + 1 ))

#define GET_SECTOR(src)	( (unsigned short) ( ( (unsigned short) src) & (unsigned short)0x3F ) )
#define GET_CYLINDER(src)  ((unsigned short) ( ((unsigned short)src) >>8 ) + (( ((unsigned short)src) & (unsigned short)0xC0 )<<2) )




typedef struct FFD_WORKER_THREAD
{
	int			fExit;
#ifdef _NT_KERNEL_
	HANDLE		hThread;
	LIST_ENTRY	IrpList;
	KSEMAPHORE	sema;
	FAST_MUTEX	mutex;
#endif

} FFD_WORKER_THREAD, *PFFD_WORKER_THREAD;





typedef enum BLOCK_STATUS
{
	BLOCK_STATUS_UNDEFINED,		// no data about block data, spare area of this block haven't been read
	BLOCK_STATUS_BAD,			// block damaged, unreadable of returns incorrect data
	BLOCK_STATUS_DIRTY,			// block is no longer in use, but should be erased before new record
	BLOCK_STATUS_FREE,			// block is free for recording, already erased
	BLOCK_STATUS_IN_USE			// block contains useful information

}BLOCK_STATUS, *PBLOCK_STATUS;

typedef struct FFD_BLOCK
{
	unsigned long	numWrites;	// number of write operations happend to this block
	unsigned short	crc;
	unsigned char	status;
	unsigned char	serial;

} FFD_BLOCK, *PFFD_BLOCK;


typedef struct MEDIA_ID
{
	unsigned char vendor;
	unsigned char nand;

} MEDIA_ID, *PMEDIA_ID;

#define NAND_CHIP_ID	0x0E6
#define NAND_CHIP_ID2	0x073
#define NAND_CHIP_ID3	0x075



typedef struct FFD_PARTITION
{
	unsigned long		length;
	unsigned long		startOffset;

} FFD_PARTITION, *PFFD_PARTITION;


typedef struct FFD_EXTENSION
{
//	unsigned short	numGoodBlocks;		// number of good blocks in disk
	unsigned short	numFreeBlocks;		// free (already erased) blocks
	unsigned short	numDirtyBlocks;		// not used, but not erased blocks
	unsigned short	numUsedBlocks;		// used for data storage blocks
	unsigned short	numBadBlocks;
	unsigned short	numCRCFailBlocks;	// blocks where CRC check failed
	unsigned short	lastUsedBlock;		// last block used by current partition
	unsigned short	numPagesPerBlock;	// sectors per block
	unsigned short	numTotalBlocks;		// Total blocks (FFD_BLOCK_COUNT)

	int				CheckDisk;			// perform disk check (CRC)

	FFD_BLOCK		blocks[MAX_FFD_BLOCK_COUNT];	// blocks status information
	unsigned short	map[MAX_FFD_BLOCK_COUNT];		// logical-to-phisical map

	FFD_PARTITION	partition;
//	unsigned char	cash[32*512];			// Now handled with malloc	
	unsigned char*	cash;

	unsigned long	minWrites;
	unsigned long	maxWrites;

	unsigned short	blocksInPartition;
	unsigned long	sectorsInPartition;

	int				dynamicWearSmooth;

#ifdef _NTDDK_

	PDEVICE_OBJECT	deviceObject;	// NT Kernel device object
	FAST_MUTEX		HardwareMutex;
	UNICODE_STRING	deviceName;
	UNICODE_STRING	dosName;

#ifdef _FFD_USE_WORKER_THREAD
	FFD_WORKER_THREAD	Worker;
#endif // _FFD_USE_WORKER_THREAD

#endif //_NTDDK_

#ifdef _RTOS_
	RTKSemaphore	HardwareMutex;
#endif

} FFD_EXTENSION, *PFFD_EXTENSION;


//int FindLeastUsedBlock( PFFD_EXTENSION ext );
//int InitBlockMap( PFFD_EXTENSION ext );


typedef struct  _BOOT_SECTOR
{
    unsigned char       bsJump[3];          // x86 jmp instruction, checked by FS
    char				bsOemName[8];       // OEM name of formatter
    unsigned short      bsBytesPerSec;      // Bytes per Sector
    unsigned char       bsSecPerClus;       // Sectors per Cluster
    unsigned short      bsResSectors;       // Reserved Sectors
    unsigned char       bsFATs;             // Number of FATs - we always use 1
    unsigned short      bsRootDirEnts;      // Number of Root Dir Entries
    unsigned short      bsSectors;          // Number of Sectors
    unsigned char       bsMedia;            // Media type - we use RAMDISK_MEDIA_TYPE
    unsigned short      bsFATsecs;          // Number of FAT sectors
    unsigned short      bsSecPerTrack;      // Sectors per Track - we use 32
    unsigned short      bsHeads;            // Number of Heads - we use 2
    unsigned long       bsHiddenSecs;       // Hidden Sectors - we set to 0
    unsigned long       bsHugeSectors;      // Number of Sectors if > 32 MB size
    unsigned char       bsDriveNumber;      // Drive Number - not used
    unsigned char       bsReserved1;        // Reserved
    unsigned char       bsBootSignature;    // New Format Boot Signature - 0x29
    unsigned long       bsVolumeID;         // VolumeID - set to 0x12345678
    char				bsLabel[11];        // Label - set to RamDisk
    char				bsFileSystemType[8];// File System Type - FAT12 or FAT16
    char				bsReserved2[448];   // Reserved
    unsigned char       bsSig2[2];          // Originial Boot Signature - 0x55, 0xAA
}   BOOT_SECTOR, *PBOOT_SECTOR;

#pragma pack(1)

// offset in MBR should be 0x01BE

typedef struct ONDISK_PARTITION
{
	unsigned char		boot_flag;
	unsigned char		begin_head;
	unsigned short		begin_sector_cylinder;
	unsigned char		file_system_type;
	unsigned char		end_head;
	unsigned short		end_sector_cylinder;
	unsigned long		begin_absolute_sector;
	unsigned long		size_sectors;

} ONDISK_PARTITION, *PONDISK_PARTITION;

#pragma pack()



MEDIA_ID media_parameters(unsigned short *numTotalBlocks);

int check( void* buf, char value, int size );

int FindLeastUsedBlock( PFFD_EXTENSION ext );

void set_read_status_mode(void);

int media_erase( int block_no );

void media_erase_start( int block_no );

int media_erase_check(void);

void media_erase_resume(void);

int media_status(void);

int media_read_spare( void* buffer, int numBytes, int numSector, int offset );

void media_initial(void);

void media_reset( void );

int media_write( void* buf, int block_no );

int media_write_spare( void* buf, int sector, int offset, int bytes_to_save );

unsigned short calculate_checksum( void* buf, int len );

int verify( void* buf, int addr );

int flash_exists( void );


// Initialization (return 0 - error)
int InitBlockMap(PFFD_EXTENSION ext);

// Write logical sectors
int WriteBuffer(PFFD_EXTENSION ext, char* data2write, int size_in_sectors, int first_sector);

// Translate logical sector to physical block#
int Log2Phis(PFFD_EXTENSION ext, int sector);

// Read physical block#
int media_read(int first_block, int numSectors, void* pBuffer);

#endif // _FFD_H
