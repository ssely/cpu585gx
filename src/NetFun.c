#define WIN32_LEAN_AND_MEAN
#define STRICT

#ifndef _NETFUNC_C
#define _NETFUNC_C

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <alloc.h>
#include <string.h>
#include <time.h>
#include <ctype.h>


#include <rttarget.h>
#include <rtk32.h>
#include <rtkmem.h>
#include <rtfiles.h>
#include <cpumoni.h>
#include <rtkeybrd.h>
#include <clock.h>
#include <socket.h>
#include <webapi.h>
#include <ftpapi.h>

#include "..\inc\globe.h"

#ifdef HOST
   #error This demo cannot run under Windows
#endif

unsigned int ReqCount=0;

char *Head="HTTP/1.1 200 Ok\r\n";
char *tData="Content-Type: application/octet-stream;\r\nContent-disposition: filename=\"%02d%02d%02d%02d.bin\"\r\nContent-Length: %d\r\nConnection: close\r\n\r\n";
char *tHTML="Content-Type: text/html\r\nConnection: close\r\n\r\n";
char *tXML="Content-Type: text/xml\r\nConnection: close\r\n\r\n";
char *Auth="HTTP/1.1 401 Unauthorized\r\nWWW-Authenticate: Basic realm=\"Authentication System\"\r\n\r\n";
static char master_psw[0x40]={0};
static char operator_psw[0x40]={0};
static DWORD LoginAddress=0UL;
static pConfig pc;
static pSensor ps;
static pCar pCars;
static char *Buffer;

/*
const FTPPathInfo SuperUsers[] = {
   { FTP_ACCESS_READ|FTP_ACCESS_WRITE, "" },
   {0}
};

// define access rights for Users
const FTPPathInfo Users[] = {
   { FTP_ACCESS_READ,  "D:\\" },
   { FTP_ACCESS_NONE, "D:\\" },
   { FTP_ACCESS_READ, "D:\\" },
   {0}
};

// define access rights for everyone else
const FTPPathInfo Anyone[] = {
   { FTP_ACCESS_READ, "D:\\" },
   {0}
};

// define the users
const FTPUserInfo UserList[] = {
   { "root",  "secret", SuperUsers },
   { "harry", "xyz",    Users      },
   { "joe",   "zyx",    Users      },
   { "guest", "",       Anyone     },
};
*/


int Send(PIO_CONTEXT io_context, char * s, int Flags)
{
   int Result;
   char * Saved = io_context->buffer_out;

   io_context->buffer_out = s;
   io_context->length_out = strlen(s);
   Result = xn_line_put(io_context, 2, Flags);
   io_context->buffer_out = Saved;
   return Result;
}

void Unescape(char * i)
{
   const char Hex[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13, 14, 15};
   char * o = i;

   if (i == 0)
      return;

   while (1)
   {
      switch (i[0])
      {
         case '+':
            o[0] = ' ';
            break;
         case '&':
         case '\0':
            o[0] = '\0';
            return;
         case '%':
            o[0] = Hex[i[1]-'0'] * 16 + Hex[i[2]-'0'];
            i+=2;
            break;
         default:
            o[0] = i[0];
            break;
      }
      i++;
      o++;
   }
}
void _sendHead(PIO_CONTEXT io_context, BYTE tp)
{
Send(io_context, Head, PUT_QUE | PUT_SEND);
	switch(tp)
		{
	case 0:Send(io_context, tHTML, PUT_QUE | PUT_SEND);
			break;
	case 1:	Send(io_context, tXML, PUT_QUE | PUT_SEND);
			break;
	case 2: Send(io_context, Auth, PUT_QUE | PUT_SEND);
			break;
	default: break;
		}
}

PFCHAR _hex2bin(WORD h, PFCHAR s)
{
WORD i, mask = 0x8000;
	for(i=0;i<0x10;i++,mask>>=1)
		s[i]=(h & mask) ? 0x31 : 0x30;
s[16]=0;
return s;
}

void reset_fun(PIO_CONTEXT io_context, PFCHAR param)
{
char *Data;
if(param==NULL)
{	
_sendHead(io_context,0);
Send(io_context, "<body bgcolor='#CCCCCC'><H2><font color='red'>Could't Reset Controller</font></H2><hr/></body>",PUT_QUE | PUT_SEND);
return;
}
if((Data = http_find_string_value(param, "pass", strlen(param)))!=NULL)
	{
	if(atoi(Data)==32760)
		{
		RTOut(0x20C,1);
		RTKDelay(1000UL);
		}
	}
return;
}

void copy_fun(PIO_CONTEXT io_context, PFCHAR param)
{
RTFHANDLE hIn,hOut;
long fSize;
UINT isRead;
if(param==NULL)
	{
	_sendHead(io_context,0);
	Send(io_context, "<body bgcolor='#CCCCCC'><H2><font color='red'>Achtung</font></H2><hr/><pre><font color='green'>No Find param file</font></pre></body>", PUT_QUE | PUT_SEND);
	return;
	}
    Unescape((char *) param);
	if(!strncmpi(param,"src=",4))
		{
		if((hIn=RTFOpen(&param[4],RTF_READ_ONLY)) <0)
			{
			_sendHead(io_context,0);
			Send(io_context, "<body bgcolor='#CCCCCC'><H2><font color='red'>Achtung</font></H2><hr/><pre><font='green'>No Find source file</font></pre>", PUT_QUE | PUT_SEND);
			return;
			}
		if((fSize = RTFSeek(hIn, 0UL, RTF_FILE_END)) > 0x100000)
			{
			_sendHead(io_context,0);
			Send(io_context, "<body bgcolor='#CCCCCC'><H2><font color='red'>Achtung</font></H2><hr/><pre><font='green'>File too long</font></pre>", PUT_QUE | PUT_SEND);
			RTFClose(hIn);
			return;
			} 
		RTFSeek(hIn, 0UL, RTF_FILE_BEGIN);
		RTFRead(hIn, Buffer,fSize, &isRead);
		RTFClose(hIn);
		param[4]='C';
		if((hIn=RTFOpen(&param[4],RTF_CREATE_ALWAYS | RTF_READ_WRITE)) <0)
			{
			_sendHead(io_context,0);
			Send(io_context, "<H2>Achtung</H2><hr/><pre>Unable open destantion file</pre>",PUT_SEND);
			Send(io_context, "<b>", PUT_QUE);
			Send(io_context, &param[4], PUT_QUE);
			Send(io_context,"</b>",PUT_QUE| PUT_SEND);
			return;
			}
		RTFWrite(hIn, Buffer, fSize, &isRead);
		RTFClose(hIn);
		_sendHead(io_context,0);
		sprintf(Buffer,"<body bgcolor='#888888'><H2>Ja Ja Naturlich!</H2><hr/><pre>Copy file <b>%s</b> to target system</pre>",&param[4]);
		Send(io_context,Buffer,PUT_QUE| PUT_SEND);
		return;	
		}
	else
		Send(io_context, "<H2>Achtung</H2><hr/><pre>No Find source param</pre>", PUT_QUE | PUT_SEND);
}

void settings_fun(PIO_CONTEXT io_context, PFCHAR param)
{
char *content,*Tmp;
DWORD Id=-1;
int i=0;
pSensor Data;
 
ReqCount++;
content=&Buffer[512*1024];
_sendHead(io_context,1);
if(param!=NULL)
	{
	while(param[i])
		{
		param[i]=(char)tolower(param[i]);
		i++;
		}
if((Tmp = http_find_string_value(param, "id", strlen(param)))!=NULL)
	{
	if((Id =(DWORD)atol(Tmp)))
		{
		Id--;
		Id&=0xF;
		Data = &ps[Id];
		if((Tmp = http_find_string_value(param, "range", strlen(param)))!=NULL)
			Data->bRange=(BYTE)atoi(Tmp);
		if((Tmp = http_find_string_value(param, "type", strlen(param)))!=NULL)
			Data->bType=(BYTE)atoi(Tmp);
		if((Tmp = http_find_string_value(param, "reverse", strlen(param)))!=NULL)
			Data->bReverse=(BYTE)atoi(Tmp);
		if((Tmp = http_find_string_value(param, "filter", strlen(param)))!=NULL)
			Data->bFType=(BYTE)atoi(Tmp);
		if((Tmp = http_find_string_value(param, "min", strlen(param)))!=NULL)
			{
			Data->bMin=(WORD)atol(Tmp);
			if(Data->bMin)
				{
				Data->coeff=(20. * ((Data->bRange+1)*0.125) - Data->bType*4 * ((Data->bRange+1)*0.125)) /(5./4096.);
				Data->coeff=(float)(Data->bMax-Data->bMin) / Data->coeff;
				}
			}
		if((Tmp = http_find_string_value(param, "max", strlen(param)))!=NULL)
			{
			Data->bMax=(WORD)atol(Tmp);
			if(Data->bMax !=0.)
				{
				Data->coeff=(20. * ((Data->bRange+1)*0.125) - Data->bType*4 * ((Data->bRange+1)*0.125)) /(5./4096.);
				Data->coeff=(float)(Data->bMax-Data->bMin) / Data->coeff;
				}

			}
		if((Tmp = http_find_string_value(param, "fs", strlen(param)))!=NULL)
			Data->Fs=atof(Tmp)/ 1000.;
		if((Tmp = http_find_string_value(param, "fx", strlen(param)))!=NULL)
			Data->Fx=atof(Tmp)/ 1000.;
		if((Tmp = http_find_string_value(param, "win", strlen(param)))!=NULL)
			Data->wWindow=(WORD)atoi(Tmp);
		if((Tmp = http_find_string_value(param, "offset", strlen(param)))!=NULL)
			Data->wOffset=(WORD)atoi(Tmp);
		if((Tmp = http_find_string_value(param, "limit", strlen(param)))!=NULL)
			Data->wSelf=(WORD)atoi(Tmp);
		}
	else	{
		
		if((Tmp = http_find_string_value(param, "onoff", strlen(param)))!=NULL)
			{
			pc->isOn=0UL;
				for(i=0;;i++)
					{
					if( !isdigit((int)Tmp[i]))
						break;
					pc->isOn<<=1;
					pc->isOn |= (Tmp[i]-0x30);
					}			
			}
		if((Tmp = http_find_string_value(param, "count", strlen(param)))!=NULL)
			pc->CarCount=(BYTE)atoi(Tmp);
		if((Tmp = http_find_string_value(param, "hz", strlen(param)))!=NULL)
			pc->RegDelay=atoi(Tmp);
		}
	
	}
}
Send(io_context, "<?xml version=\"1.0\" encoding=\"windows-1251\"?><settings>",PUT_QUE | PUT_SEND);
sprintf(content,"<global>\
<count comment=\"���-�� ������� � �����\">%u</count>\
<onoff comment=\"����� ��������� ������� Hex 0-����� �������� 1-�������\">%02X</onoff>\
<hz comment=\"������� ������������� Hz\">%u</hz></global>",(WORD)pc->CarCount,(WORD)pc->isOn,pc->RegDelay);
Send(io_context, content,PUT_QUE | PUT_SEND);
if(Id==-1)
	{
	i=0;Id=16;
	}
else{
	i=Id;Id++;
	}
for(i;i<Id;i++)
{
Data = &ps[i];
sprintf(content,"<sensor Id=\"%u\">",i+1);
Send(io_context, content,PUT_QUE | PUT_SEND);
sprintf(content, "<range comment=\"������������� ����� 0/1 - 125��/250��\">%u</range>\
<type comment=\"��� ������� 0/1 - 0-20mA/4-20mA\">%u</type>\
<reverse comment=\"������/����������� ������ 0/1\">%u</reverse>\
<order comment=\"��� ������� 0-��������� ������ 1-���������� ������\">%u</order>\
<min comment=\"������� �������\">%u</min>\
<max comment=\"�������� �������\">%u</max>\
<limit comment=\"����� ������������ ������� �����.(Y)\">%u</limit>\
<fs comment=\"������� ������ ����������� Hz\">%.1f</fs>\
<fx comment=\"������� ������ ��������� Hz\">%.1f</fx>\
<win comment=\"������ ���� ������� �����.(X)\">%u</win>\
<offset comment=\"�������� �� ������ ��������� ������� �����.(X)\">%u</offset>\
<coeff comment=\"����������� ���������\">%.4f</coeff></sensor>",
(BYTE)Data->bRange,(BYTE)Data->bType,(BYTE)Data->bReverse,(BYTE)Data->bFType,
(WORD)Data->bMin,(WORD)Data->bMax,Data->wSelf,Data->Fs,Data->Fx,Data->wWindow,
(WORD)Data->wOffset,(float)Data->coeff);
Send(io_context, content,PUT_QUE | PUT_SEND);
}	
Send(io_context, "</settings>",PUT_QUE | PUT_SEND);
}

void current_fun(PIO_CONTEXT io_context, PFCHAR param)
{
char *Data,*content;
int i,ii=0, j=0;
BYTE shift=255;
pCar CarTmp;
SYSTEMTIME isT;

ReqCount++;
content=&Buffer[0x400];

_sendHead(io_context,1);
Send(io_context, "<?xml version=\"1.0\" encoding=\"windows-1251\"?><items>",PUT_QUE | PUT_SEND);
CarTmp = &pCars[pc->cCount];
if(CarTmp->isT.wYear)
	{
	sprintf(content,"<item id=\"%u\" orient=\"right\" dt=\"%04d-%02d-%02d %02d:%02d:%02d\" lt=\"%lu\"><record>%u</record><channel_on type=\"bin\">%s</channel_on><stateword type=\"bin\">%s</stateword>",
	CarTmp->Id,CarTmp->isT.wYear,CarTmp->isT.wMonth,CarTmp->isT.wDay,CarTmp->isT.wHour,CarTmp->isT.wMinute,CarTmp->isT.wSecond,CarTmp->Diff,pc->cCount+1,_hex2bin((WORD)pc->isOn, &Buffer[0x100]),_hex2bin((WORD)CarTmp->isError, Buffer));
	Send(io_context, content,PUT_QUE | PUT_SEND);
	for(i=0;i<2;i++)
		{
		sprintf(content,"<rol num=\"%d\"><dhk  prev=\"%.2f\">%.2f</dhk><rzpk prev=\"%.2f\">%.2f</rzpk><rzpr prev=\"%.2f\">%.2f</rzpr><param a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/><code a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/></rol>",++ii,
	CarTmp->ldhk[i+2],CarTmp->ldhk[i],CarTmp->lrzpk[i+2],CarTmp->lrzpk[i],CarTmp->lrzpr[i+2],CarTmp->lrzpr[i],CarTmp->lparms[0+(4*i)],CarTmp->lparms[1+(4*i)],CarTmp->lparms[2+(4*i)],CarTmp->lparms[3+(4*i)],CarTmp->lcode[0+(4*i)],CarTmp->lcode[1+(4*i)],CarTmp->lcode[2+(4*i)],CarTmp->lcode[3+(4*i)]);
		Send(io_context, content,PUT_QUE | PUT_SEND);
		}
	for(i=0;i<2;i++)
		{
		sprintf(content,"<rol num=\"%d\"><dhk prev=\"%.2f\">%.2f</dhk><rzpk prev=\"%.2f\">%.2f</rzpk><rzpr prev=\"%.2f\">%.2f</rzpr><param a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/><code a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/></rol>",++ii,
	CarTmp->rdhk[i+2],CarTmp->rdhk[i],CarTmp->rrzpk[i+2],CarTmp->rrzpk[i],CarTmp->rrzpr[i+2],CarTmp->rrzpr[i],CarTmp->rparms[0+(4*i)],CarTmp->rparms[1+(4*i)],CarTmp->rparms[2+(4*i)],CarTmp->rparms[3+(4*i)],CarTmp->rcode[0+(4*i)],CarTmp->rcode[1+(4*i)],CarTmp->rcode[2+(4*i)],CarTmp->rcode[3+(4*i)]);
		Send(io_context, content,PUT_QUE | PUT_SEND);
		}
	Send(io_context, "</item>",PUT_QUE | PUT_SEND);
	}
sprintf(content,"<speed>%.2f</speed><stoptime>%02d:%02d:%02d %02d-%02d-%04d</stoptime><starttime>%02d:%02d:%02d %02d-%02d-%04d</starttime><waittime>%.2f</waittime></items>",pc->cSpeed,pc->StopTime.wHour,pc->StopTime.wMinute,pc->StopTime.wSecond,pc->StopTime.wDay,pc->StopTime.wMonth,pc->StopTime.wYear,pc->StartTime.wHour,pc->StartTime.wMinute,pc->StartTime.wSecond,pc->StartTime.wDay,pc->StartTime.wMonth,pc->StartTime.wYear,(float)pc->WaitTime/100.);
Send(io_context, content,PUT_QUE | PUT_SEND);
//Send(io_context, "</items>",PUT_QUE | PUT_SEND);
}

void getall_fun(PIO_CONTEXT io_context, PFCHAR param)
{
char *content;
int i,ii, j;
pCar CarTmp;
SYSTEMTIME isT;
ReqCount++;
content=&Buffer[0x128*1024];


_sendHead(io_context,1);
ii=pc->cCount;
CarTmp = &pCars[ii];
sprintf(content,"<?xml version=\"1.0\" encoding=\"windows-1251\"?><items current=\"%u\">",CarTmp->isT.wYear ? pc->cCount+1 : CarTmp->isT.wYear);
Send(io_context,content,PUT_QUE | PUT_SEND);
for(j=0; j < pc->CarCount-1; j++)
	{
CarTmp = &pCars[ii];
if(CarTmp->isT.wYear)
	{
	sprintf(content,"<item id=\"%u\" orient=\"right\" dt=\"%04d-%02d-%02d %02d:%02d:%02d\" lt=\"%lu\"><record>%u</record><channel_on type=\"bin\">%s</channel_on><stateword type=\"bin\">%s</stateword>",
	CarTmp->Id,CarTmp->isT.wYear,CarTmp->isT.wMonth,CarTmp->isT.wDay,CarTmp->isT.wHour,CarTmp->isT.wMinute,CarTmp->isT.wSecond,CarTmp->Diff,ii+1,_hex2bin((WORD)pc->isOn, &Buffer[0x100]),_hex2bin((WORD)CarTmp->isError, Buffer));
	Send(io_context, content,PUT_QUE | PUT_SEND);
	for(i=0;i<2;i++)
		{
		sprintf(content,"<rol num=\"%d\"><dhk prev=\"%.2f\">%.2f</dhk><rzpk prev=\"%.2f\">%.2f</rzpk><rzpr prev=\"%.2f\">%.2f</rzpr><param a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/><code a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/></rol>",1+i,
	CarTmp->ldhk[i+2],CarTmp->ldhk[i],CarTmp->lrzpk[i+2],CarTmp->lrzpk[i],CarTmp->lrzpr[i+2],CarTmp->lrzpr[i],CarTmp->lparms[0+(4*i)],CarTmp->lparms[1+(4*i)],CarTmp->lparms[2+(4*i)],CarTmp->lparms[3+(4*i)],CarTmp->lcode[0+(4*i)],CarTmp->lcode[1+(4*i)],CarTmp->lcode[2+(4*i)],CarTmp->lcode[3+(4*i)]);
		Send(io_context, content,PUT_QUE | PUT_SEND);
		}
	for(i=0;i<2;i++)
		{
		sprintf(content,"<rol num=\"%d\"><dhk prev=\"%.2f\">%.2f</dhk><rzpk prev=\"%.2f\">%.2f</rzpk><rzpr prev=\"%.2f\">%.2f</rzpr><param a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/><code a=\"%.2f\" b=\"%.2f\" c=\"%.2f\" d=\"%.2f\"/></rol>",3+i,
	CarTmp->rdhk[i+2],CarTmp->rdhk[i],CarTmp->rrzpk[i+2],CarTmp->rrzpk[i],CarTmp->rrzpr[i+2],CarTmp->rrzpr[i],CarTmp->rparms[0+(4*i)],CarTmp->rparms[1+(4*i)],CarTmp->rparms[2+(4*i)],CarTmp->rparms[3+(4*i)],CarTmp->rcode[0+(4*i)],CarTmp->rcode[1+(4*i)],CarTmp->rcode[2+(4*i)],CarTmp->rcode[3+(4*i)]);
		Send(io_context, content,PUT_QUE | PUT_SEND);
		}
	Send(io_context, "</item>",PUT_QUE | PUT_SEND);
	}
	else
		break;
	ii--;
	if(ii<0)
		ii=pc->CarCount-1;
	}
sprintf(content,"<speed>%.2f</speed><stoptime>%02d:%02d:%02d %02d-%02d-%04d</stoptime><starttime>%02d:%02d:%02d %02d-%02d-%04d</starttime><waittime>%.2f</waittime></items>",pc->cSpeed,pc->StopTime.wHour,pc->StopTime.wMinute,pc->StopTime.wSecond,pc->StopTime.wDay,pc->StopTime.wMonth,pc->StopTime.wYear,pc->StartTime.wHour,pc->StartTime.wMinute,pc->StartTime.wSecond,pc->StartTime.wDay,pc->StartTime.wMonth,pc->StartTime.wYear,(float)pc->WaitTime/100.);
Send(io_context, content,PUT_QUE | PUT_SEND);
}

void demon_fun(PIO_CONTEXT io_context, PFCHAR param)
{
RTFHANDLE hIn;
RTFFileInfo fInf;
DWORD fSize,i,j,k;
UINT isRead;
BYTE * Buffer,*Saved;
char * content;

ReqCount++;
content=&Buffer[1024*1024-1024];


sprintf(content,"%sdata.bin",pc->FtpRoot);
if((hIn=RTFOpen(content,RTF_READ_ONLY)) <0)
	{
	_sendHead(io_context,0);
	Send(io_context, "<body bgcolor='black'><H2><font color='red'>Achtung</font></H2><hr/><pre><font color='green'>No Find data file</font></pre></body>", PUT_QUE | PUT_SEND);
	return;
	}
RTFGetFileInfo(hIn, &fInf);

fSize = fInf.DirEntry->FileSize;
if(fSize<(1024*1024-1024))
	RTFRead(hIn, Buffer,fSize, &isRead);
if(isRead!=fSize)
	{
	_sendHead(io_context,0);
	Send(io_context, "<body bgcolor='black'><H2><font color='red'>Achtung</font></H2><hr/><pre><font color='green'>Read Fault</font></pre></body>", PUT_QUE | PUT_SEND);
	RTFClose(hIn);
	return;
	}
sprintf(content,tData,fInf.DirEntry->DateTime.Day,fInf.DirEntry->DateTime.Month,fInf.DirEntry->DateTime.Hour,fInf.DirEntry->DateTime.Minute,fSize);
RTFClose(hIn);
Send(io_context, Head, PUT_QUE | PUT_SEND);
Send(io_context, content, PUT_QUE | PUT_SEND);
j=fSize / 1024UL;
k=0UL;
for(i=0;i<j;i++)
	{
   Saved = io_context->buffer_out;
   io_context->buffer_out =&Buffer[k];
   io_context->length_out = 1024;
   isRead = xn_line_put(io_context, 2, PUT_QUE | PUT_SEND);
   io_context->buffer_out = Saved;
	k+=1024UL;
	}
i=fSize % 1024UL;
   Saved = io_context->buffer_out;
   io_context->buffer_out =&Buffer[k];
   io_context->length_out = i;
   isRead = xn_line_put(io_context, 2, PUT_QUE | PUT_SEND);
   io_context->buffer_out = Saved;
}
/*
int login(PIO_CONTEXT io_context)
{
int i,rc=0,sz;
char *p,*a,*b;
RTFHANDLE file_in;
UINT Read;
sockaddr_in sa;
sz=sizeof(sockaddr_in);

LoginAddress=0UL;
for(i=0;i<io_context->end_offset_in-4;i++)
        {
		if(io_context->pb_in[i]==0xA)
			p=&io_context->pb_in[i+1];
		if(!io_context->pb_in[i])
			io_context->pb_in[i]=0x20;
		}
if(p!=NULL)
	{
	a=strtok(p,"\x20");
	b=strtok(NULL,"\x20");
	p=strtok(NULL,"\x20\x0");
	if(!strcmpi(a,"Authorization:"))
		if(!strcmpi(b,"Basic"))
				rc=2;
	}
if(rc!=2)
	return 0;

if(!master_psw[0])
	{
	if((file_in=RTFOpen("c:\\confiq.sys",RTF_READ_ONLY))>0)
		{
		if(RTFRead(file_in,(void *) master_psw,0x40,&Read)<0)
			{
			RTFClose(file_in);
			return 0;
			}
		RTFClose(file_in);
		}
	else
		{
        if((file_in=RTFOpen("c:\\confiq.sys",RTF_CREATE))<0)
				return 0;
		else if(rc==2)
				{
				if(RTFWrite(file_in, p, strlen(p) & 0x3F, &Read)<0)
					{
					RTFClose(file_in);
					return 0;
					}
				strcpy(master_psw,p);
				RTFClose(file_in);
				}
		}
	}
i = getpeername(io_context->sock,&sa,&sz);
if(!strcmpi(p,master_psw))
	{
	if(!i)
		LoginAddress=sa.sin_addr.s_un.S_addr;
	return 1;
	}
if(!operator_psw[0])
   if((file_in=RTFOpen("c:\\avtoexec.bat",RTF_READ_ONLY))>0)
    {
	if(RTFRead(file_in,(void *) operator_psw,0x40,&Read)<0)
			{
			RTFClose(file_in);
			return 0;
			}
	RTFClose(file_in);
	}
if(Read)
	if(!strcmpi(p,operator_psw))
		{
	if(!i)
		LoginAddress=sa.sin_addr.s_un.S_addr;
		return 1;
		}
return 0;
} 

void setup_fun(PIO_CONTEXT io_context, PFCHAR param)
{
int i,sz;
char *p=NULL, *a,*b;
sockaddr_in sa;
sz=sizeof(sockaddr_in);

i = getpeername(io_context->sock,&sa,&sz);
if(!login(io_context))
	Send(io_context, Auth, PUT_QUE | PUT_SEND);
else if((!i) && (LoginAddress==sa.sin_addr.s_un.S_addr))
	{
	Send(io_context, "<pre>Hello</pre>", PUT_QUE | PUT_SEND);
	LoginAddress=0UL;
	}
}
*/
BOOL InitNetFunc(pConfig isCfg, pSensor isSen, pCar isCar)
{
BOOL rc = FALSE;
pc = isCfg;
ps = isSen;
pCars = isCar;
if((Buffer = _rtkMEMAlloc(1024UL*1024UL))!=NULL)
	rc=TRUE;
return rc;
}
void  CloseNetFunc(void)
{
_rtkMEMFree(Buffer);
}

#endif //_NETFUNC_C