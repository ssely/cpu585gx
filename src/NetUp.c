#define WIN32_LEAN_AND_MEAN
#define STRICT

#ifndef _NETUP_C
#define _NETUP_C

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <alloc.h>
#include <string.h>
#include <time.h>
#include <ctype.h>


#include <rttarget.h>
#include <rtk32.h>
#include <rtkmem.h>
#include <rtfiles.h>
#include <cpumoni.h>
#include <rtkeybrd.h>
#include <clock.h>
#include <socket.h>
#include <webapi.h>
#include <ftpapi.h>

#include "netcfg.h"
#include "..\inc\globe.h"

#ifdef HOST
   #error This demo cannot run under Windows
#endif


int interface = SOCKET_ERROR;

struct get_function_entry get_function_table[] = 
{
  {"load.pl", demon_fun},
  {"copy.pl", copy_fun},
  {"getcurrent.xml",current_fun},
  {"getall.xml",  getall_fun},
  {"settings.xml",settings_fun},
  {"reset.pl", reset_fun},
  {	"",             END_OF_TABLE}
};

struct post_function_entry post_function_table[] =
{
  
  {"",             END_OF_TABLE}
};
void Error(const char * Msg)
{
   printf("%s, error code: %s\n", Msg, xn_geterror_string(WSAGetLastError()));
   exit(1);
}

void InterfaceCleanup(void)
{
   if (interface != SOCKET_ERROR)
   {
      const int One = 1;
      xn_interface_opt(interface, IO_HARD_CLOSE, (const char *)&One, sizeof(int));
      xn_interface_close(interface);
      interface = SOCKET_ERROR;
   #if DEVICE_ID == PRISM_PCMCIA_DEVICE
      RTPCShutDown();
   #endif
   }
}

void char2ip(BYTE * Arr, char *dat)
{
int i,j=0;
if(Arr==NULL)
	return;
memset(Arr,0,4);
	for(i=0;i<strlen(dat);i++)
		{
		if(isdigit(dat[i]))
			Arr[j]=Arr[j]*10+(dat[i]-'0');
		if(dat[i]=='.') j++;
		}
}
void NetUp(LPSTR isIP,LPSTR Mask, LPSTR Gw, LPSTR Dns)
{
   int Result;
   #if _MSC_VER >= 1400               // Keep MSC 8.0 run-time library happy
      RTT_Init_MSC8CRT();
   #endif
   char2ip(TargetIP,isIP);
   char2ip(NetMask, Mask);
   char2ip(DefaultGateway, Gw);
   char2ip(DNSServer,Dns);	


//   if (!RTKDebugVersion())            // switch of all diagnostics and error messages of RTIP-32
      xn_callbacks()->cb_wr_screen_string_fnc = NULL;

   KBInit();
   CLKSetTimerIntVal(10*1000);        // 10 millisecond tick
   RTKDelay(10);
   CPUMonitorStart(CPU_IDLE_TASK);    // we want load dstatistics
   RTCMOSSetSystemTime();             // get the right time-of-day

   XN_REGISTER_FTP_SRV()
   XN_REGISTER_WEB_SRV()              // register the Web server add-on

   CFG_NTCPPORTS = 16;                // preallocate a few more ports
   
   CFG_NUM_PACKETS3 = 128;            // need lot's of buffers to support many connections
   CFG_ARPCLEN = 32;
  CFG_FTP_MAX_SESSIONS=16;
   Result = xn_rtip_init();           // Initialize the RTIP stack
   if (Result == SOCKET_ERROR)
      Error("xn_rtip_init failed");
   RTKDelay(100);
   atexit(InterfaceCleanup);                          // make sure the driver is shut down properly

//   RTCallDebugger(RT_DBG_CALLRESET, (DWORD)exit, 0);  // even if we get restarted by the debugger

   Result = BIND_DRIVER(MINOR_0);     // tell RTIP what Ethernet driver we want (see netcfg.h)
   if (Result == SOCKET_ERROR)
      Error("driver initialization failed");

   // Open the interface
   interface = xn_interface_open_config(DEVICE_ID, MINOR_0, ED_IO_ADD, ED_IRQ, ED_MEM_ADD);
   if (interface == SOCKET_ERROR)
      Error("xn_interface_open_config failed");
   else
   {
      struct _iface_info ii;
      xn_interface_info(interface, &ii);
      printf("Interface opened, MAC address: %02x-%02x-%02x-%02x-%02x-%02x\n",
         ii.my_ethernet_address[0], ii.my_ethernet_address[1], ii.my_ethernet_address[2],
         ii.my_ethernet_address[3], ii.my_ethernet_address[4], ii.my_ethernet_address[5]);
   }

   // Set the IP address and interface
   printf("Using static IP address %i.%i.%i.%i\n", TargetIP[0], TargetIP[1], TargetIP[2], TargetIP[3]);
   Result = xn_set_ip(interface, TargetIP, NetMask);
   // define default gateway and DNS server
  // xn_rt_add(RT_DEFAULT, ip_ffaddr, DefaultGateway, 1, interface, RT_INF);
   xn_rt_add(RT_DEFAULT, NetMask, DefaultGateway, 1, interface, RT_INF);
   xn_set_server_list((DWORD*)DNSServer, 1);


   if (Result != 0)
      Error("TCP/IP stack initialization failed");
}

void WebRun(LPSTR Doc,LPSTR Ftp)
{
int Result;
   vf_init();

if(Doc!=NULL)
    CFG_WEB_ROOT_DIR = Doc;
else
   CFG_WEB_ROOT_DIR = "C:\\";

if(Ftp!=NULL)
	CFG_FTP_ROOT_DIR = Ftp;
else
	CFG_FTP_ROOT_DIR = "C:\\";
   CFG_WEB_TIMEOUT = 2;
   CFG_MOD_DATE = 1;

   // start the Web server
 RTKRTLCreateThread((RTKThreadFunction) ftp_server_daemon, 0, 4096, 0, NULL, "FTP Master");
 RTKRTLCreateThread((RTKThreadFunction)http_server_daemon, 0, 4096, 0, NULL, "Web Master");
}
#endif //_NETUP_C