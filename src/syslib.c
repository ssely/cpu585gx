#ifndef _SYS_LIB_C
#define _SYS_LIB_C
#include <rttarget.h>
#include <math.h>
#ifndef _GLOBE_H
#include <../inc/globe.h>
#endif

#define WIDTH 32

BOOL R_TRIG32(BYTE Ch,BOOL iCLK)
{
static BOOL lM[WIDTH]={TRUE};
BOOL lRc;
if(Ch >=WIDTH)
	Ch=WIDTH-1;
lRc= iCLK && !lM[Ch];
lM[Ch]=iCLK;
return lRc;
}

BOOL F_TRIG32(BYTE Ch,BOOL iCLK)
{
static BOOL lM[WIDTH]={0};
BOOL lRc;
if(Ch >=WIDTH)
	Ch=WIDTH-1;
lRc= !iCLK && !lM[Ch];
lM[Ch]=!iCLK;
return lRc;
}

void _setBit(unsigned long * Data, unsigned char Num)
{
unsigned long mask=1;
*Data |=(mask<<Num);
}

void _resetBit(unsigned long * Data, unsigned char Num)
{
unsigned long mask=1;
mask<<=Num;
*Data &=(~mask);
}

BOOL _isBit(unsigned long  Data, unsigned char Num)
{
unsigned long mask=1;
mask<<=Num;
return (Data & mask);
}

void FilterHz (float * in, float * out, float Fs, float Fx, WORD Hz, WORD N, WORD sizeIn)
{
int i,j;
float Fd; //������� ������������� ������� ������
float Fc;
float H [1024] = {0}; //���������� �������������� �������
float H_id [1024] = {0}; //��������� ���������� ��������������
float W [1024] = {0}; //����������� ����
float SUM=0;
Fd = (float)Hz;
//������ ���������� ���-�� �������
Fc = (Fs + Fx) / (2 * Fd);
for (i=0; i < N; i++)
{
if (i==N/2) 
	H_id[i] = 2*M_PI*Fc;
else 
	H_id[i] = ( sin( 2*M_PI*Fc*(i-N/2) ) ) / (i-N/2);

W [i] = 0.42 - 0.5 * cos((2*M_PI*i) / N) + 0.08 * cos((4*M_PI*i) / N);
H [i] = H_id[i] * W[i];
SUM = SUM + H[i];
}

//���������� ���������� ��������������
for (i=0; i<N; i++) H[i]= H[i] / SUM;

//���������� ������� ������
for (i=0; i<sizeIn-N; i++)
	{
	out[i]=0.;
	for (j=0; j<N; j++) 
		out[i]=out[i] + H[j]*in[(int)N + i-j];
	}
}

void FilterLP(float *in, float * out, float Alpha, WORD sizeIn)
{
int i;
out[0]=in[0];
	for(i=1;i<sizeIn;i++)
		out[i] = out[i-1] + ((in[i]+in[i-1])/2 - out[i-1]) * Alpha;

}

void _fillData(rArray r,vArray v, pSensor p, pConfig cf, int count)
{
short i,ii,iii,j;
for(i=0;i<16;i++)
	memmove((void *)r[i],r[i]+100, 1100 * sizeof(float));

	for(ii=0;ii<100;ii++)
		{
		for(i=0;i<2;i++)
			for(iii=0;iii<8;iii++)
				{
				j=(iii+(i<<3));			
				r[j][1100+ii] = (float)v[i][ii][iii];
				}
		}

	for(i=0; i<count; i++)
		if(_isBit(cf->isOn,i))
			FilterHz(p[i].pData,p[i].pFlt, p[i].Fs,p[i].Fx,cf->RegDelay,p[i].wWindow,1200); 
}
#endif