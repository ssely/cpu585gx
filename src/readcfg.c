#ifndef _READCFG_C_
#define _READCFG_C_

#include <rttarget.h>
//#include <rtfiles.h>
#include <rtk32.h>
#include <rtkflt.h>
#include <rtkmem.h>


#include <stdlib.h>
#include <string.h>
#include "..\inc\globe.h"
#include "..\inc\wxml.h"

BOOL _clearconfig(pConfig  p)
{
		 free(p->WebRoot);
		 free(p->FtpRoot);
		 free(p->ipaddr);
		 free(p->mask);
		 free(p->dns);
		 free(p->gateway);
_rtkMEMFree(p->Mp);
return TRUE;
}

BOOL _readconfig(char * fname, pConfig  p)
{
int cc;
yxml_t *fxml,*cfg_xml,*sec_xml;
	if((fxml = yxml_read_file((const char *) fname,&cc))==NULL)
		return FALSE;
	if((cfg_xml = yxml_find(fxml,"config_server"))==NULL)
		{
		yxml_free(fxml);
		return FALSE;
		}
	if((sec_xml = yxml_find(cfg_xml->details,"DocumentRoot"))!=NULL)
		 p->WebRoot=strdup(sec_xml->details->name);
	if((sec_xml = yxml_find(cfg_xml->details,"ftproot"))!=NULL)
		 p->FtpRoot=strdup(sec_xml->details->name);
	if((sec_xml = yxml_find(cfg_xml->details,"targetIp"))!=NULL)
		 p->ipaddr=strdup(sec_xml->details->name);
	if((sec_xml = yxml_find(cfg_xml->details,"netmask"))!=NULL)
		 p->mask=strdup(sec_xml->details->name);
	if((sec_xml = yxml_find(cfg_xml->details,"dns"))!=NULL)
		 p->dns=strdup(sec_xml->details->name);
	if((sec_xml = yxml_find(cfg_xml->details,"gateway"))!=NULL)
		 p->gateway=strdup(sec_xml->details->name);
	if((sec_xml = yxml_find(cfg_xml->details,"wrkchannel"))!=NULL)
		{
		p->isOn=0;
		for(cc=0;cc<16;cc++)
			{
			p->isOn <<=1;
			p->isOn |=(sec_xml->details->name[cc]-0x30);
			}
		}
yxml_free(fxml);
p->Mp= _rtkMEMAlloc(1024UL*1024UL);
p->RegDelay=50UL;
p->dwWork=0UL;
p->EqBaseOne=400;
p->EqBaseTwo=300;
p->CarCount=141;
return TRUE;
}
#endif
//_rtkMEMAlloc(2048UL*1024UL))!=NULL)
//_rtkMEMFree(isValue.Buffer);
