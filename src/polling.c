#ifndef _POLLING_C
#define _POLLING_C

#define WIN32_LEAN_AND_MEAN
#define STRICT

#include <windows.h>
#include <stdio.h>

#include <rttarget.h>
#include <rtk32.h>
#include <rtcom.h>
#include <rtkmem.h>
#include <rtfiles.h>

#include "..\inc\globe.h"

#define DataChannel 30
#define WegChannel  31

#define MAXC		12500
#define INT    		0x0002	// ��� ��������� ����������
#define TMR    		0x0010	// ��� ��������� �������
#define BANK   		0x0020	// ��� ����=1
#define FIFO    	0x0800	// ��� ��������� ������ � FIFO
#define SHARE    	0x2000
#define NWR			0x01000

#define RS_FIFO 	0x10    // ��� Reset FIFO
#define END	        0x9     // ��� ������� ������� FIFO



WORD BASE_1, BASE_2;
WORD perodic;
pConfig pc;
pSensor ps;
short * virtual;
float * real;
WORD periodic;
pCar Cars;
RTFHANDLE fIn=NULL;

BOOL _InitPolling(pConfig cfg, pSensor Sen, pCar pC, vArray v, rArray r, WORD a1, WORD a2)
{
BOOL rc=FALSE;
pc=cfg;
ps=Sen;
Cars = pC;
virtual = (short *) v;
real = (float *) r;
periodic =  (WORD) cfg->RegDelay;
BASE_1=a1;
BASE_2=a2;
pc->Buffer = _rtkMEMAlloc(0x100);
if(pc->Buffer!=NULL)
	rc=TRUE;
return rc;
}

_ClearPolling(void)
{
_rtkMEMFree(pc->Buffer);
return;
}
void RTKAPI Polling(void * P)
{
static DWORD tCount=0UL, tcurr;
BYTE ii;
SYSTEMTIME isT; 
int Error;
WORD wDIV;
#ifdef DEBUG
wDIV=2500;
#else 
wDIV = (WORD)((float)(100./(float)periodic) * 10000.); 
#endif

	while(TRUE)
	{
	RTKWait(EndIRQ);
	tcurr=GetTickCount();
//#ifdef DEBUG
//	printf("P1=%04X\tP2=%04X\tLatency=%lu\tTime=%lu\tDelay=%u\n",BASE_1,BASE_2,_getLatency(1),tcurr-tCount,pc->RegDelay);
//#endif
	tCount=tcurr;
	RTOut(BASE_1+11,RS_FIFO|END);
	RTOutW(BASE_1+4,wDIV);
	if(BASE_2)
		{
		RTOut(BASE_2+11,RS_FIFO|END);
		RTOutW(BASE_2+4,wDIV);
		}
	if(!(BASE_2))
		RTOutW(BASE_1+0, SHARE|FIFO|TMR|INT|BANK);
	else{
		RTOutW(BASE_1+0, SHARE|FIFO|TMR|INT|BANK);
		RTOutW(BASE_2+0, SHARE|FIFO|TMR|INT|BANK);
		}
		RTCMOSReadTime(&isT);
#ifdef DEBUG
		_getFtpData(virtual,READ);
#endif
		sprintf(pc->Buffer,"%sdataout.bin",pc->FtpRoot);
		if((fIn = RTFOpen(pc->Buffer,RTF_READ_WRITE | RTF_CREATE_ALWAYS))>0L)
			{
			RTFWrite(fIn,(void *) virtual,sizeof(vArray), &Error);
			RTFClose(fIn);
			}
		sprintf(pc->Buffer,"%sfilter.bin",pc->FtpRoot);

		_fillData(real,virtual, ps, pc, 16);
		if((fIn = RTFOpen(pc->Buffer,RTF_READ_WRITE | RTF_CREATE_ALWAYS))>0L)
			{
			for(ii=0;ii<16;ii++)
				RTFWrite(fIn, ps[ii].pFlt,800*sizeof(float), &Error);
			RTFClose(fIn);
			}
		 watch_cycle(pc, ps, Cars,16);

	}
}
#endif // _POLLING_C