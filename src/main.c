#ifndef _MAIN_C
#define _MAIN_C

#define WIN32_LEAN_AND_MEAN
#define STRICT

#include <windows.h>
#include <stdio.h>
#include <alloc.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <mem.h>

#include <rttarget.h>
#include <rtfiles.h>
#include <rtk32.h>
#include <rtkflt.h>
#include <rtkmem.h>
#include <cpumoni.h>
#include <rtkmem.h>
#include <rtkeybrd.h>
#include <clock.h>
#include <socket.h>
#include <webapi.h>

#include "..\inc\ffd.h"
#include "..\inc\wxml.h"
#include "..\inc\globe.h"



DWORD AddAddr;
WORD BaseAddr1=0,BaseAddr2=0;
RTKTaskHandle PollingHandle;

static RTFileSystem Console = { RT_FS_CONSOLE, 0, 0, &RTConsoleFileSystem };

#ifdef RTFILE_VER
//RTFiles-32 FAT-12/16/32 and ISO 9660 file system, drive letters 'A'-'P'
RTFileSystem FATFiles = { RT_FS_FILE | RT_FS_IS_DEFAULT,0x000000F , 0x000000F, &RTFilesFileSystem };
#endif

// RAM/ROM file system on driver letter 'R'
RTFileSystem RAMFiles = { RT_FS_FILE, 1 << ('R'-'A'), 0, &RTRAMFileSystem };


// the list of all installed file systems
RTFileSystem * RTFileSystemList[] = {
   &Console,
#ifdef RTFILE_VER
   &FATFiles,
#endif
   &RAMFiles,
   NULL
};

extern RTFDriver RTFDrvFFD;
extern RTFDevice RTFDeviceList[];

static RTFDrvIDEData IDEDriveCData = {0};
static RTFDrvRAMData RAMDriveDData = {8*1024*1024/512};
static FFD_EXTENSION  FFDData = {0};


RTFDevice RTFDeviceList[] =
{ 
	{ RTF_DEVICE_FDISK,  0, 0, &RTFDrvFFD,	 &FFDData }, // ���������� FLASH-����
//	{ RTF_DEVICE_FDISK , 2, 0, &RTFDrvIDE, &IDEDriveCData }, // Compact-Flash
	{ RTF_DEVICE_FDISK , 0, 0, &RTFDrvRAM, &RAMDriveDData }, // RAM-����
	{ 0 }
};

int SaveFile(void * user_pointer, BYTE * buffer, int bufflen)
{
/*	if(memmove((BYTE *)user_pointer+AddAddr,buffer,bufflen))
		{
		 AddAddr+=bufflen;
		 return bufflen;
		}
	else
		return -1;
*/
	memmove((BYTE *)user_pointer+AddAddr,buffer,bufflen);
	AddAddr+=bufflen;
return AddAddr;
}

BOOL _getFtpData(short * Data, BYTE flags )
{
static struct ftpcli fc;
char Username[] = "anonymous";
char Password[] = "s_selyanin@zsmk.ru";
BOOL rc = FALSE;
static short Tmp[8]={0};

static RTFHANDLE fh=NULL;
int Result;
static WORD i=0;

//RTKDelay(1);

if(flags==INIT)
		{
		fh = RTFOpen("dataout.bin",RTF_READ_ONLY|RTF_CACHE_DATA);
		i=0;
		}
//if((i++)>=200)
	if((flags==INIT) || (flags==READ))
			RTFRead(fh,(void *) Data,3200, &Result);

if(flags==SHUTDOWN)
			RTFClose(fh);
/*
if(Result && (i>=200))
	{
	i=0;
	return TRUE;
	}
else 
	return FALSE;
*/
return rc;
}


BOOL _initSensor(int count, pSensor *ps, pConfig pc, rArray ra)
{
int i;
*ps =  _rtkMEMAlloc(count*sizeof(Sensor));

	for(i=0;i<count;i++)
		{
		(*ps)[i].pData = (float *) ra[i];
		(*ps)[i].pFlt = _rtkMEMAlloc(1200 * sizeof(float));
		(*ps)[i].Fs=.1f;
		(*ps)[i].Fx=1.f;
		(*ps)[i].wWindow=128;
		(*ps)[i].bMin=50;
		(*ps)[i].bMax=300;
		(*ps)[i].bRange=1;
		(*ps)[i].bType=1;
		(*ps)[i].coeff=(20. * (((*ps)[i].bRange+1)*0.125) - (*ps)[i].bType*4 * (((*ps)[i].bRange+1)*0.125)) /(5./4096.);
		(*ps)[i].coeff=(float)((*ps)[i].bMax-(*ps)[i].bMin) / (*ps)[i].coeff;
		(*ps)[i].UpTime=RTKGetTime();
		(*ps)[i].isMin=(*ps)[i].fData[0]=(*ps)[i].fData[0]=10000.;
		(*ps)[i].isActive=0;
		switch(i)
			{
			case 2: (*ps)[i].wOffset=144;
			break;
			case 4: (*ps)[i].wOffset=112;
			break;
			case 5: (*ps)[i].wOffset=104;
			break;
			case 6: (*ps)[i].wOffset=256;
			break;
			case 8: (*ps)[i].wOffset=224;
			break;
			case 11:(*ps)[i].wWindow=32;
			        (*ps)[i].wOffset=0;
			break;
			default: (*ps)[i].wOffset=0;
			break;
			}
		(*ps)[i].isActive=0;
		if(i > 7)
			(*ps)[i].wSelf=1024;
		else									
			(*ps)[i].wSelf=2000;					
		}
RTCMOSReadTime(&(pc->StopTime));
RTCMOSReadTime(&(pc->StartTime));
pc->WaitTime=RTKGetTime();
return TRUE;
}

BOOL _clearSensor(int count,pSensor *ps, pConfig pc)
{
int i;
	for(i=0; i < count; i++)
		_rtkMEMFree((*ps)[i].pFlt);
_rtkMEMFree(*ps);
return TRUE;				
}

//_rtkMEMAlloc(2048UL*1024UL))!=NULL)
//_rtkMEMFree(isValue.Buffer);

void  main(void)
{
BOOL cc=FALSE;
Config cfg={0};
pSensor pSen;
pCar pCars;
vArray vA={0};
rArray rA={0};
WORD a1,a2;
char Buffer[0x100];
int Error,ii,rc;
RTFHANDLE fIn=NULL,fHandl=NULL;
RTFDOSDirEntry isE;

cc = RTFFormat("\\\\.\\D:",0,NULL,RTF_FMT_FAT_16);
RTSetFlags(RT_MM_VIRTUAL | RT_CLOSE_FIND_HANDLES, 1);


       cc = _readconfig("R:\\param.xml", &cfg);
       cc = _initSensor(16, &pSen, &cfg, rA);
	  NetUp(cfg.ipaddr,cfg.mask,cfg.gateway,cfg.dns);
	  KBInit();
	  _getFtpData((short *)vA,INIT);
	  pCars = _rtkMEMAlloc(256UL*sizeof(Car));
	  memset((void *) pCars,0,256UL*sizeof(Car));
	  InitNetFunc(&cfg, pSen, pCars);
  	  WebRun(cfg.WebRoot,cfg.FtpRoot);
  	  _InitADCModule((short *)vA);
  	  _getADCAddr(&a1 ,&a2);
  	  _InitPolling(&cfg,pSen,pCars,(short *)vA, (float *) rA, a1, a2);
  	  SetupADC(5,a1,a2,cfg.RegDelay);
  	  PollingHandle = RTKRTLCreateThread(Polling,4,32768,0,NULL,"Polling Task");
  	while(!KBKeyPressed())
  	{
//    	if(_getFtpData((short *)vA,READ))
    		{
/*	   		_fillData(rA,vA,pSen,&cfg,16);
			sprintf(cfg.Mp,"%sfilter.bin",cfg.FtpRoot);
			fIn = RTFOpen(cfg.Mp,RTF_READ_WRITE | RTF_CREATE_ALWAYS);
			RTFWrite(fIn, pSen[1].pFlt,1200*sizeof(float), &Error);
			RTFClose(fIn);*/
//			rc = watch_cycle(&cfg, pSen, pCars,16);
			}
	RTKDelay(100);
	}
	RTKTerminateTask(&PollingHandle);
	CleanADC(5,a1,a2);
	CloseNetFunc();
	_ClearPolling();
	_rtkMEMFree(pCars);
	_getFtpData((short *)vA,SHUTDOWN);		
	_clearSensor(16,pSen, &cfg);
		
cc = _clearconfig(&cfg);
return;
}
#endif //_MAIN_C