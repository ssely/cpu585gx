#ifndef _MWORKS_C
#define _MWORKS_C

#include <rttarget.h>
#include <rtk32.h>
#include <rtkflt.h>
#include <rtkmem.h>

#include <math.h>

#include "..\inc\globe.h"

#define SAVEONE 0x11
#define ISWORK 0x12

#define OFFSET 200

BYTE  fill_cars(pConfig p, pSensor ps , pCar pc,RTKTime Df, BOOL *isFill)
{
static BYTE carcount=0;
BYTE rc;
int i,ii;
float A,B,C,D,correct=5./4096.;
rc=carcount;

if(!(p->dwWork & 0xFFFF0000))
		{
		*isFill=FALSE;
		return rc;
		}
ii=0;
pc[carcount].isError=(p->dwWork>>16);
for(i=0; i < 16; i++)
		{
		if((ps[i].fData[0] > 5000.) || (ps[i].fData[0] < 800.)) ps[i].fData[0]=4096.;
		if((ps[i].fData[1] > 5000.) || (ps[i].fData[1] < 800.)) ps[i].fData[1]=4096.;
#ifdef DEBUG
//		printf("%u\t%.2f\t%.2f\n",i,ps[i].fData[0],ps[i].fData[1]);
#endif
	if(_isBit(pc[carcount].isError,(BYTE) i))
			ii++;
		}
if(ii < 3)
	{
	*isFill=FALSE;
	return rc;
	}
#ifdef DEBUG
//	printf("d[0]=%.2f d[1]=%.2f d[2]=%.2f d[3]=%.2f d[4]=%.2f\n",ps[0].fData[0],ps[1].fData[0],ps[2].fData[0],ps[3].fData[0]);
#endif
		 	


RTCMOSReadTime(&(pc[carcount].isT));
pc[carcount].Diff=Df;
	for(ii=0; ii<2; ii++)
		for(i=0; i<2; i++)
			{
			A=ps[0+(ii<<2)].fData[i];B=ps[1+(ii<<2)].fData[i];C=ps[2+(ii<<2)].fData[i];D=ps[3+(ii<<2)].fData[i];
			switch(ii)
				{
				case 0:
				pc[carcount].lcode[(i<<2)]=A/*+(ps[0].bType/(5./4096))*/;
				A-=(float)ps[0].bType / correct;
				A=pc[carcount].lparms[(i<<2)]=ps[0].bMin + A*ps[0].coeff;
				pc[carcount].lcode[(i<<2)+1]=B /*+(ps[1].bType/(5./4096))*/;
				B-=(float)ps[1].bType / correct;
				B=pc[carcount].lparms[(i<<2)+1]=ps[1].bMin +  B*ps[1].coeff;
				pc[carcount].lcode[(i<<2)+2]=C /*+(ps[2].bType/(5./4096))*/;
				C-=(float)ps[2].bType / correct;
				C=pc[carcount].lparms[(i<<2)+2]=ps[2].bMin +  C*ps[2].coeff;
				pc[carcount].lcode[(i<<2)+3]=D /*+(ps[3].bType/(5./4096))*/;
				D-=(float)ps[3].bType / correct;
				D=pc[carcount].lparms[(i<<2)+3]=ps[3].bMin +  D*ps[3].coeff;
				pc[carcount].ldhk[i+2]=pc[carcount].ldhk[i];
				pc[carcount].lrzpk[i+2]=pc[carcount].lrzpk[i];
				pc[carcount].lrzpr[i+2]=pc[carcount].lrzpr[i];
				pc[carcount].ldhk[i]=p->EqBaseOne-A;
				pc[carcount].lrzpk[i]=(B+C-A-(p->EqBaseOne-p->EqBaseTwo))/2.f-50.f;
				pc[carcount].lrzpr[i]=(B-C)/2.f;
				break;
				case 1:
				pc[carcount].rcode[(i<<2)]=A /*+(ps[0].bType/(5./4096))*/;
				A-=(float)ps[0].bType / correct;
				A=pc[carcount].rparms[(i<<2)]= ps[0].bMin +  A*ps[0].coeff;
				pc[carcount].rcode[(i<<2)+1]=B/*+(ps[1].bType/(5./4096))*/;
				B-=(float)ps[1].bType / correct;
				B=pc[carcount].rparms[(i<<2)+1]=ps[1].bMin + B *ps[1].coeff;
				pc[carcount].rcode[(i<<2)+2]=C /*+(ps[2].bType/(5./4096))*/;
				C-=(float)ps[2].bType / correct;
				C=pc[carcount].rparms[(i<<2)+2]=ps[2].bMin +  C*ps[2].coeff;
				pc[carcount].rcode[(i<<2)+3]=D /*+(ps[3].bType/(5./4096))*/;
				D-=(float)ps[3].bType / correct;
				D=pc[carcount].rparms[(i<<2)+3]=ps[3].bMin +  D*ps[3].coeff;
				pc[carcount].rdhk[i+2]=pc[carcount].rdhk[i];
				pc[carcount].rrzpk[i+2]=pc[carcount].rrzpk[i];
				pc[carcount].rrzpr[i+2]=pc[carcount].rrzpr[i];
				pc[carcount].rdhk[i]=p->EqBaseOne-A;
				pc[carcount].rrzpk[i]=(B+C-A-(p->EqBaseOne-p->EqBaseTwo))/2.f;
				pc[carcount].rrzpr[i]=(B-C)/2.f;
				break;
				default:
				break;
				}
			}
carcount++;
if(carcount == p->CarCount)
	carcount=0;
*isFill=TRUE;
return rc;				
}

BOOL isDown(BYTE chan,BYTE ctl,pSensor ps, int count)
{
int i;
BOOL rc = FALSE;
float fMin;
fMin = ps[chan].isMin; 
	for(i=count; i<100; i++)
		{
		ps[chan].isMin = min( ps[chan].isMin, ps[chan].pFlt[ps[chan].wOffset+OFFSET+i]);
		if(fMin > ps[chan].isMin)
			{
			ps[chan].isAvg = (ps[chan].pFlt[ps[chan].wOffset+OFFSET+i-1]+ps[chan].pFlt[ps[chan].wOffset+OFFSET+i]) / 2.;
			fMin = ps[chan].isMin;
#ifdef DEBUG
//			printf("%u.\tMin=%.4f\tAvg=%.4f\tOffset=%.4f\n",chan,ps[chan].isMin,ps[chan].isAvg, ps[chan].pFlt[ps[chan].wOffset+OFFSET+i]);
#endif
			}
		if((rc = F_TRIG32(chan ,ps[chan].pFlt[ps[chan].wOffset+OFFSET+i] < (float)ps[chan].wSelf)))
			break;
		if(ps[ctl].pFlt[ps[chan].wOffset+OFFSET+i] < (float)(ps[ctl].wSelf))
			break;

		}	
return rc;					
}

BOOL isUp(BYTE ch, BYTE ctl, pSensor ps, int *count)
{
int i;
BOOL rc = FALSE;

	for(i=0; i<100; i++)
	{
		if((rc = R_TRIG32(ch ,ps[ch].pFlt[ps[ch].wOffset+OFFSET+i] < (float) ps[ch].wSelf)))
			{
			*count = i;
			break;
			}
	if(ps[ctl].pFlt[ps[ch].wOffset+OFFSET+i] < (float)(ps[ctl].wSelf))
			break;
	}				
return rc;					
} 
int watch_cycle(pConfig p, pSensor ps, pCar pc,int count)
{
static BOOL Flop = TRUE,isStop=FALSE,isGo=FALSE;
static RTKTime isTime=0UL;
BOOL prn;
int rc = 0,i,j=0;
float Data;
RTKTime DiffTime;

Data = (ps[10].pFlt[ps[10].wOffset+OFFSET]-819.2)*(7.46/(4096.-819.2));
p->cSpeed = (Data > 0.) ? Data : 0.f;

#ifdef DEBUG
//		if(p->cSpeed >0.001)
//			printf("Speed = %.2f\n",p->cSpeed);
#endif

	if(isUp(11,11,ps,&j))
		{
		p->WaitTime=RTKGetTime()-isTime;
		if( p->WaitTime > 1000UL)
			{
			RTCMOSReadTime(&(p->StopTime));
			isStop=TRUE;
			}
		isTime=RTKGetTime();
//		return rc;
		}

	if(isStop)
		{
		if(ps[11].pFlt[ps[11].wOffset+800] > (ps[11].wSelf))
			{
			isStop=FALSE;
			RTCMOSReadTime(&(p->StartTime));
			}
//		else
//			return rc; 
		}

	for(i=0; i<count; i++)
		{
		if(_isBit(p->isOn, (BYTE)i))
			{
			j=0;
			if(ps[11].pFlt[ps[i].wOffset+OFFSET] <= (ps[11].wSelf)) // �� ���� ���� ���� ����� �� ��������
				continue;
			rc++;

			if(isUp((BYTE) i ,11,ps,&j))
				{
				if(!isGo)
					{
					DiffTime = RTKGetTime() - ps[i].UpTime;
					if((abs(DiffTime) > 400UL) && (abs(DiffTime) < 1200UL))
						{
						 isGo=TRUE;
#ifdef DEBUG
						 printf("Begin Count = %lu\n",DiffTime);
#endif
						RTCMOSReadTime(&(p->StartTime));
						p->dwWork=0x0;
						}
					else
						{
#ifdef DEBUG
						printf("%u\tWait Car = %lu\n",i,DiffTime);
#endif
						ps[i].UpTime=RTKGetTime();
						}
					}
				if((!_isBit(p->dwWork, (BYTE)i)) && isGo)
					{
					if(isGo)
						{
						DiffTime = RTKGetTime() - ps[i].UpTime;
						Flop=TRUE;
						 _setBit(&(p->dwWork),(BYTE) i);
#ifdef DEBUG
 						 printf("Up1channel = %u\tData = %.2f\tTimer = %lu\n",i,ps[i].pFlt[ps[i].wOffset+OFFSET+j],DiffTime);
#endif
 						 ps[i].isActive=1;
 					 	}
					 ps[i].UpTime=RTKGetTime();
					}
				else if((_isBit(p->dwWork, (BYTE) i)) && isGo)
					{
						Flop=FALSE;
						_setBit(&(p->dwWork),(BYTE) (i+count));
#ifdef DEBUG
 						printf("Up2Channel = %u\tData = %.3f\tTime = %lu\tErr = %04X\n",i,ps[i].pFlt[ps[i].wOffset+OFFSET+j],RTKGetTime()-ps[i].UpTime,(WORD)p->dwWork);
#endif
						ps[i].UpTime=RTKGetTime();
						ps[i].isActive=2;

					}
				}

			if(R_TRIG32(SAVEONE,Flop))
						{

						p->cCount = fill_cars(p, ps ,pc, DiffTime, &prn);
						if(DiffTime > 1200UL)
							{
							isGo=FALSE;
							if(p->WaitTime > 1000UL)
								p->cCount = fill_cars(p, ps ,pc, DiffTime, &prn);
							}
						p->dwWork&=0xFFFF0000;
						if(prn)
							{
#ifdef DEBUG
							printf("%u.%u %08X Min[0] = %.3f Min[1] = %.3f Diff=%lu\n",i,ps[i].isActive,p->dwWork,ps[i].fData[0],ps[i].fData[1],DiffTime);
#endif
							}
						_setBit(&(p->dwWork),(BYTE) i);
						}
			if(F_TRIG32(SAVEONE,Flop))
						{
						p->dwWork&=0xFFFF;
#ifdef DEBUG
//						printf("%u.%u %08X Min[0] = %.3f Min[1] = %.3f diff = %.3f\n",i,ps[i].isActive,p->dwWork,ps[i].fData[0],ps[i].fData[1],(ps[i].fData[0]-ps[i].fData[1])*ps[i].coeff);
#endif
						_setBit(&(p->dwWork),(BYTE) (i+count));
						}
			if(ps[i].isActive)
				{
				if(ps[i].isActive==1)
					{
#ifdef DEBUG
//				printf("%u.%u %08X <-> %08X Min[0] = %.3f Min[1] = %.3f diff = %.3f\n",i,ps[i].isActive,p->dwError, p->dwWork,ps[i].fData[0],ps[i].fData[1],(ps[i].fData[0]-ps[i].fData[1])*ps[i].coeff);
#endif
	 				_resetBit(&(p->dwWork),(BYTE) (i+count));
					}
				if(ps[i].isActive==2)
					{
#ifdef DEBUG
//				printf("%u.%u %08X <-> %08X Min[0] = %.3f Min[1] = %.3f diff = %.3f\n",i,ps[i].isActive,p->dwError, p->dwWork,ps[i].fData[0],ps[i].fData[1],(ps[i].fData[0]-ps[i].fData[1])*ps[i].coeff);
#endif
					_resetBit(&(p->dwWork),(BYTE) i);
                    }

				if(isDown(i,11,ps,j))
					{

					ps[i].fData[ps[i].isActive-1]=ps[i].isAvg;
#ifdef DEBUG
//					printf("%u-%u\t data_1=%.2f data_2=%.2f\n",i,ps[i].isActive,ps[i].fData[0],ps[i].fData[1]);
#endif
					ps[i].isMin=10000.;
					ps[i].isActive=0;

					}
				}
			}	
		}
return rc;
}
				 
#endif //_MWORKS_C

