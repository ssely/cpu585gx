#ifndef _ADC_C
#define _ADC_C
#include <rttarget.h>
#include <rtk32.h>
#include <rtkmem.h>
#include "..\inc\globe.h"

#define MAXC		16000
#define INT    		0x0002	// ��� ��������� ����������
#define TMR    		0x0010	// ��� ��������� �������
#define BANK   		0x0020	// ��� ����=1
#define FIFO    	0x0800	// ��� ��������� ������ � FIFO
#define SHARE    	0x2000
#define NWR			0x01000
#define RS_FIFO 	0x10    // ��� Reset FIFO
#define END	        0x9     // ��� ������� ������� FIFO
#define FIFO_END	0x80    // ���������� ����������� ������� FIFO


RTKSemaphore EndIRQ;

WORD IRQMask;
unsigned int Nfifo=(512+END*32);
static WORD BASE1=0,BASE2=0;
RTKIRQDescriptor old_int;
int IRQ=5;
int count=0;
short *psData;
static DWORD TmrTick=0UL,TTick;

DWORD _getLatency(BYTE Fl)
{
TTick=TmrTick;
if(Fl)
	TmrTick=0UL;
return TTick;
}

void _InitADCModule(short *d)
{
psData = d;
}

int _getADCAddr(WORD* a1,WORD* a2)
{
extern WORD BASE1,BASE;
int i;
char c,d;
unsigned short rc=0;
*a1=0U;
*a2=0U;
    for (i=0x110;i<0x400;i+=0x10)
		{
	     c = RTIn(i+0xE);
	     d = RTIn(i+0xF);
		if (c=='A'&&(d==87||d==17))
			{
			 if(!rc)
				{
				*a1=BASE1=i;
				EndIRQ = RTKCreateSemaphore(ST_BINARY,0,"IRQ 5 Sema");
				rc++;
				}
			else{
				*a2=BASE2=i;
				rc++;
				}

				
			}
		}
return rc;
}

void RTKAPI int_dio(void)
{
extern int IRQ;
extern int count;
short i;
volatile WORD Data,Data_1,Data_2;
volatile RTKTime IrqTime,Tmp=0;
WORD isB=1;
    Data=RTIn(BASE1+11);
	if(BASE2)
	    Data_1=RTIn(BASE1+11);
	if(RTIn(BASE1+11) & FIFO_END)
		isB=BASE1;
	else{
        RTKIRQEnd(IRQ);
		TmrTick++;
        return;
		}
if(isB!=1)
	{
	RTOutW(BASE1,BANK);
	if(BASE2)
            RTOutW(BASE2,BANK);
	for(i=0;i<Nfifo;i++)
		{
		if(Data & FIFO_END)
			isB = psData[i]=RTInW(BASE1+12);
		if(BASE2)
			if(Data_1 & FIFO_END)
				isB = psData[i+Nfifo]=RTInW(BASE2+12);
	        }
		RTKSignal(EndIRQ);
		TmrTick++;
	}
RTKIRQEnd(IRQ);
}


void SetupADC(int Irq, WORD Ba1,WORD Ba2,WORD periodic)
{
extern RTKIRQDescriptor  old_int;
extern int IRQ;
WORD IRQMask,Dat,wDIV;
WORD BA;
wDIV = (WORD)((float)(100./(float)periodic) * 10000.); 
BA=Ba1;//+0x400;
IRQMask=1<<Irq;
IRQ=Irq;
  RTOutW(BA+4, wDIV);
if(Ba2)
  RTOutW(Ba2+4, wDIV);

  RTOutW(BA+6,0);
if(Ba2)
  RTOutW(Ba2+6,0);
	
  RTOutW(BA+8,0);
if(Ba2)
  RTOutW(Ba2+8,0);
  RTKSaveIRQHandlerFar(Irq,&old_int);
  RTKSetIRQHandler(Irq,int_dio);
  RTKEnableIRQ(Irq);
TmrTick=0UL;
  RTOut(BA+11,RS_FIFO|END);// Reset FIFO
Dat = RTIn(BA+11);
if(Ba2)
  RTOut(Ba2+11,RS_FIFO|END);// Reset FIFO

  RTOutW(BA+0, SHARE|FIFO|TMR|INT|BANK);
if(Ba2)
  RTOutW(Ba2+0, SHARE|FIFO|TMR|INT|BANK);
}

void CleanADC(int Irq, WORD Ba1,WORD Ba2)
{
extern  RTKIRQDescriptor old_int;

	RTOut(Ba1,BANK);
if(Ba2)
	RTOut(Ba2,BANK);

RTKDisableIRQ(Irq); 
RTKRestoreIRQHandlerFar(Irq,&old_int);
RTKDeleteSemaphore(&EndIRQ);
}
#endif // _ADC_C